package com.example.restapi;

import android.content.ContentValues;
import android.webkit.URLUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class RequestHttpURLConnection {

    public String request(String _url, Map<String, String> header, ContentValues _params, String requestMethod, int timeOut) throws Exception {

        StringBuilder sbParams = new StringBuilder();

        if (_params == null)
            sbParams.append("");
        else {
            boolean isAnd = false;
            String key;
            String value;

            for (Map.Entry<String, Object> parameter : _params.valueSet()) {
                key = parameter.getKey();
                value = parameter.getValue().toString();

                if (isAnd)
                    sbParams.append("&");

                sbParams.append(key).append("=").append(value);

                if (!isAnd)
                    if (_params.size() >= 2)
                        isAnd = true;
            }
        }

        HttpURLConnection urlConn;
        URL url = new URL(_url);
        if (URLUtil.isHttpsUrl(_url))
            urlConn = (HttpsURLConnection) url.openConnection();
        else
            urlConn = (HttpURLConnection) url.openConnection();

        urlConn.setReadTimeout(timeOut);
        urlConn.setConnectTimeout(timeOut);
        urlConn.setRequestMethod(requestMethod);

        if (header != null)
            for (Map.Entry<String, String> item : header.entrySet())
                urlConn.setRequestProperty(item.getKey(), item.getValue());

        if (!requestMethod.equals("GET")) {
            PrintWriter pw = new PrintWriter(new OutputStreamWriter(urlConn.getOutputStream()));
            pw.write(sbParams.toString());
            pw.flush();
            pw.close();
        }

        if (urlConn.getResponseCode() != HttpsURLConnection.HTTP_OK)
            throw new Exception(String.valueOf(urlConn.getResponseCode()));

        BufferedReader reader = new BufferedReader(new InputStreamReader(urlConn.getInputStream(), "UTF-8"));

        String line;
        StringBuilder page = new StringBuilder();
        while ((line = reader.readLine()) != null)
            page.append(line);

        return page.toString();
    }
}