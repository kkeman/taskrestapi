package com.example.restapi;

import android.content.ContentValues;
import android.os.Handler;
import android.os.Looper;

import org.json.JSONObject;

import java.util.Map;

public class AsyncApi {

    public static final String REQUEST_METHOD_GET = "GET";
    public static final String REQUEST_METHOD_POST = "POST";
    public static final String REQUEST_METHOD_PUT = "PUT";
    public static final String REQUEST_METHOD_DELETE = "DELETE";

    private String mUrl;
    private Map<String, String> mHeader = null;
    private ContentValues mParam;
    private String mRequestMethod;
    private int mTimeout = 5000;
    private CallbackObjectResponse mCallback;

    public interface CallbackObjectResponse {
        void onResponse(JSONObject result);

        void onError(Exception error);
    }

    public AsyncApi(String url, String requestMethod, ContentValues params) {
        mUrl = url;
        mRequestMethod = requestMethod;
        mParam = params;
    }

    public void setHeader(Map<String, String> header) {
        mHeader = header;
    }

    public void setTimeout(int timeout) {
        mTimeout = timeout;
    }

    public void setCallback(CallbackObjectResponse callback) {
        mCallback = callback;
    }


    public void connect() {
        runnable.run();
    }

    public void connectAsync() {
        new Thread(runnable).start();
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                RequestHttpURLConnection requestHttpURLConnection = new RequestHttpURLConnection();
                JSONObject result;

                String resultString = requestHttpURLConnection.request(mUrl, mHeader, mParam, mRequestMethod, mTimeout);

                result = new JSONObject(resultString);

                new Handler(Looper.getMainLooper()).post(() -> mCallback.onResponse(result));
            } catch (Exception e) {
                mCallback.onError(e);
            }
        }
    };
}
