package com.example.restapi;

import android.content.ContentValues;
import android.util.Log;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    String URL_HOST_HTTPS = "https://en.wikipedia.org/api/rest_v1/page";
    String URL_HOST_HTTP = "http://en.wikipedia.org/api/rest_v1/page";

    String API_HTML = "/html";
    String API_SUMMARY = "/summary";
    String API_RELATED = "/related";

    int mTimeout = 10000;

    String search = "/love";

    @Test
    public void setAPI() {
        startAPI(URL_HOST_HTTPS + API_SUMMARY + search, AsyncApi.REQUEST_METHOD_GET, mTimeout,null, null);
    }

    private void startAPI(String url, String requestMethod, int timeout, ContentValues params, Map<String, String> header) {

        AsyncApi asyncApi = new AsyncApi(url, requestMethod, params);
        asyncApi.setTimeout(timeout);
        asyncApi.setHeader(header);
        asyncApi.setCallback(new AsyncApi.CallbackObjectResponse() {
            @Override
            public void onResponse(JSONObject result) {
                Log.w("test", "https_GET result : " + result);
                assertTrue(true);
            }

            @Override
            public void onError(Exception error) {
                Log.w("test", "https_GET Exception : ", error);
                fail();
            }
        });
        asyncApi.connect();
    }
}